#!/bin/sh
#
# art-description: Athena runs topoclustering from an ESD file
# art-type: grid
# art-include: master/Athena

athena eflowRec/run_ESDStandardReco.py | tee temp.log
echo "art-result: ${PIPESTATUS[0]}"

./test_postProcessing_Errors.sh temp.log
