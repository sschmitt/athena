/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
#include "IOVDbTestAlg/IOVDbTestAlg.h"
#include "IOVDbTestAlg/IOVDbTestCoolDCS.h"

DECLARE_COMPONENT( IOVDbTestAlg )
DECLARE_COMPONENT( IOVDbTestCoolDCS )

